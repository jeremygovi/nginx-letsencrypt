upstream upstream_%PROXY_BACKEND_NAME% {
    server %PROXY_BACKEND_NAME%;
}

server {
        listen *:80;
        server_name %SSL_VHOST_ALT_LIST%;
	location ~ /.well-known/acme-challenge/ {
                root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
                allow all;
                default_type "text/plain";
        }
	location / {
		return 301 $scheme://%SSL_VHOST%$request_uri;
	}
}

server {
        listen *:443 ssl;
        server_name %SSL_VHOST_ALT_LIST%;
	ssl_certificate /etc/letsencrypt/live/%SSL_VHOST%/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/%SSL_VHOST%/privkey.pem;
        location ~ /.well-known/acme-challenge/ {
                root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
                allow all;
                default_type "text/plain";
        }
        location / {
                return 301 $scheme://%SSL_VHOST%$request_uri;
        }
}

server {
        listen *:80;
        server_name %SSL_VHOST%;
	location ~ /.well-known/acme-challenge/ {
        	root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
        	allow all;
        	default_type "text/plain";
	}

        location / {
                proxy_pass              http://upstream_%PROXY_BACKEND_NAME%:%PROXY_BACKEND_PORT%/$uri;
                proxy_pass_request_headers              on;
                proxy_set_header        Host            $host;
                proxy_set_header        X-Real-IP       $remote_addr;
                proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_connect_timeout   30s;
                proxy_read_timeout      300s;
                proxy_send_timeout      300s;
        }

	access_log /var/log/nginx/%SSL_VHOST%_access.log main;
	error_log /var/log/nginx/%SSL_VHOST%_error.log;
}

server {
	listen *:443 ssl;
        ssl_certificate /etc/letsencrypt/live/%SSL_VHOST%/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/%SSL_VHOST%/privkey.pem;
        server_name %SSL_VHOST%;
        
	location ~ /.well-known/acme-challenge/ {
                root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
                allow all;
                default_type "text/plain";
        }

        location / {
		proxy_pass              http://upstream_%PROXY_BACKEND_NAME%:%PROXY_BACKEND_PORT%/$uri;
                proxy_pass_request_headers              on;
		proxy_set_header        Host            $host;
                proxy_set_header        X-Real-IP       $remote_addr;
                proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_connect_timeout   30s;
                proxy_read_timeout      300s;
                proxy_send_timeout      300s;
        }

        access_log /var/log/nginx/%SSL_VHOST%_access.log main;
        error_log /var/log/nginx/%SSL_VHOST%_error.log;        
}



