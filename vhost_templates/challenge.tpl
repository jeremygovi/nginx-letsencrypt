server {
        listen *:80;
        server_name %SSL_VHOST_LIST%;
	location ~ /.well-known/acme-challenge/ {
        	root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
        	allow all;
        	default_type "text/plain";
	}
}
