server {
        listen *:80;
        server_name %SSL_VHOST_ALT_LIST%;
	location ~ /.well-known/acme-challenge/ {
                root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
                allow all;
                default_type "text/plain";
        }
	location / {
		return 301 $scheme://%SSL_VHOST%$request_uri;
	}
}

server {
        listen *:443 ssl;
        server_name %SSL_VHOST_ALT_LIST%;
	ssl_certificate /etc/letsencrypt/live/%SSL_VHOST%/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/%SSL_VHOST%/privkey.pem;
        location ~ /.well-known/acme-challenge/ {
                root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
                allow all;
                default_type "text/plain";
        }
        location / {
                return 301 $scheme://%SSL_VHOST%$request_uri;
        }
}

server {
        listen *:80;
        server_name %SSL_VHOST%;
	root /var/www/%SSL_VHOST%;
	index index.php index.html
	location ~ /.well-known/acme-challenge/ {
        	root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
        	allow all;
        	default_type "text/plain";
	}
        location / {
                try_files $uri $uri/ /index.php?$args;
        }
        location ~\.php {
                fastcgi_pass %PHP_BACKEND_NAME%:%PHP_BACKEND_PORT%;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_param PHP_VALUE "error_log=/var/log/nginx/%SSL_VHOST%.php-fpm.error.log";
                fastcgi_param PHP_VALUE "newrelic.appname=%SSL_VHOST%";
                fastcgi_connect_timeout 600s;
                fastcgi_send_timeout    600s;
                fastcgi_read_timeout    600s;
                fastcgi_buffers 256 4k;
                include fastcgi_params;
        }
        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
                expires 1h;
        }
	access_log /var/log/nginx/%SSL_VHOST%_access.log main;
	error_log /var/log/nginx/%SSL_VHOST%_error.log;
}

server {
	listen *:443 ssl;
        ssl_certificate /etc/letsencrypt/live/%SSL_VHOST%/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/%SSL_VHOST%/privkey.pem;
        server_name %SSL_VHOST%;
        root /var/www/%SSL_VHOST%;
	index index.php index.html       
	location ~ /.well-known/acme-challenge/ {
                root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
                allow all;
                default_type "text/plain";
        }
        location / {
                try_files $uri $uri/ /index.php?$args;
        }
        location ~\.php {
                fastcgi_pass %PHP_BACKEND_NAME%:%PHP_BACKEND_PORT%;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_param PHP_VALUE "error_log=/var/log/nginx/%SSL_VHOST%.php-fpm.error.log";
                fastcgi_param PHP_VALUE "newrelic.appname=%SSL_VHOST%";
                fastcgi_connect_timeout 600s;
                fastcgi_send_timeout    600s;
                fastcgi_read_timeout    600s;
                fastcgi_buffers 256 4k;
                include fastcgi_params;
        }
        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
                expires 1h;
        }
        access_log /var/log/nginx/%SSL_VHOST%_access.log main;
        error_log /var/log/nginx/%SSL_VHOST%_error.log;        
}



