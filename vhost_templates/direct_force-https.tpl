server {
        listen *:80;
        server_name %SSL_VHOST% %SSL_VHOST_ALT_LIST%;
	location ~ /.well-known/acme-challenge/ {
                root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
                allow all;
                default_type "text/plain";
        }
	location / {
		return 301 https://%SSL_VHOST%$request_uri;
	}
}

server {
        listen *:443 ssl;
        server_name %SSL_VHOST_ALT_LIST%;
	ssl_certificate /etc/letsencrypt/live/%SSL_VHOST%/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/%SSL_VHOST%/privkey.pem;
        location ~ /.well-known/acme-challenge/ {
                root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
                allow all;
                default_type "text/plain";
        }
        location / {
                return 301 https://%SSL_VHOST%$request_uri;
        }
}

server {
	listen *:443 ssl;
        ssl_certificate /etc/letsencrypt/live/%SSL_VHOST%/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/%SSL_VHOST%/privkey.pem;
        server_name %SSL_VHOST%;
        root /var/www/%SSL_VHOST%;
        location ~ /.well-known/acme-challenge/ {
                root         %LETS_ENCRYPT_CHALLENGE_FILE_DIR%;
                allow all;
                default_type "text/plain";
        }
        access_log /var/log/nginx/%SSL_VHOST%_access.log main;
        error_log /var/log/nginx/%SSL_VHOST%_error.log;        
}



