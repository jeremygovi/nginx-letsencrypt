#!/bin/bash
#set -e
#set -x

RELOAD_FLAG_FILE="/tmp/NEED_NGINX_RELOAD.txt"

output() {
        echo "$(date +"[%Y-%m-%d %T]") $1 "
}

crash_container() {
        pkill nginx
        exit 255
}

reload_nginx() {
        output "Reloading Nginx service"
        /etc/init.d/nginx configtest
        if [ $? -eq 0 ]; then
                /etc/init.d/nginx reload
        else
                output "Error in Nginx configtest! Exiting."
                crash_container
        fi
}


##########################################################
##							##
##	Script begin					##
##							##
##########################################################

# Sleeping a moment at the beginning. Let the time to the certs creation daemon to create all the necessary
sleep 300

while true
do
	output "Checking for domain renewal"
	# certbot renew --force-renewal --renew-hook "/usr/bin/touch $RELOAD_FLAG_FILE"
	certbot renew -q --renew-hook "/usr/bin/touch $RELOAD_FLAG_FILE"
	if [ -f $RELOAD_FLAG_FILE ]; then
		output "One or more domains has been renewed. Nginx need to be reloaded."
		rm $RELOAD_FLAG_FILE
		reload_nginx
	else
		output "No domain renewed."
	fi
        output "Check done. Sleeping 1h before recheck... ZzzZZzzZZzz...."
        sleep 3600
done
