#!/bin/bash
#set -e
#set -x

LETS_ENCRYPT_CHALLENGE_FILE_DIR=/tmp/temp_www
LETS_ENCRYPT_CERTS_DIR=/etc/letsencrypt
NGINX_VHOSTS_DIR="/etc/nginx/sites-enabled"
RSA_KEY_SIZE=4096

output() {
        echo "$(date +"[%Y-%m-%d %T]") $1 "
}

init() {
	mkdir -p $LETS_ENCRYPT_CHALLENGE_FILE_DIR
}

crash_container() {
        pkill nginx
        exit 255
}

reload_nginx() {
	output "Reloading Nginx service"
	/etc/init.d/nginx configtest
	if [ $? -eq 0 ]; then
		/etc/init.d/nginx reload
	else
		output "Error in Nginx configtest! Exiting."
		crash_container
	fi
}

clean_temp_vhost_file() {
	rm $CHALLENGE_FILE
}

setup_variables() {
	VHOST_ALT_VARIABLE_NAME="SSL_VHOST_${i}_ALT"
	eval SSL_VHOST_ALT=\$$VHOST_ALT_VARIABLE_NAME
	if [ "$SSL_VHOST_ALT" != "" ]
	then
		output "Vhost alternative domain names set: $SSL_VHOST_ALT"
		SSL_VHOST_ALT_LIST=`echo "$SSL_VHOST_ALT" | sed s/","/" "/g`
		SSL_VHOST_LIST=`echo "$SSL_VHOST $SSL_VHOST_ALT_LIST"`
	else
		output "No vhost alternative domain names are set"
		SSL_VHOST_ALT_LIST=""
		SSL_VHOST_LIST=$SSL_VHOST
	fi

	VHOST_EMAIL_VARIABLE_NAME="SSL_VHOST_${i}_EMAIL"
	eval SSL_VHOST_EMAIL=\$$VHOST_EMAIL_VARIABLE_NAME
	if [ "$SSL_VHOST_EMAIL" == "" ]
	then
		output "No Email provided ! You must set up env variable \"SSL_VHOST_EMAIL\" Can't continue. Exiting."
		exit 1
	fi

	VHOST_MODE_VARIABLE_NAME="SSL_VHOST_${i}_MODE"
	eval SSL_VHOST_MODE=\$$VHOST_MODE_VARIABLE_NAME

	VHOST_FORCE_HTTPS_VARIABLE_NAME="SSL_VHOST_${i}_FORCE_HTTPS"
        eval SSL_VHOST_FORCE_HTTPS=\$$VHOST_FORCE_HTTPS_VARIABLE_NAME
	if [ "$SSL_VHOST_FORCE_HTTPS" == "yes" ]
	then
		output "Force HTTPS: http will be redirected to https"
		SSL_VHOST_FORCE_HTTPS="force-https"
	else
		output "HTTPS not forced: http and https requests will be allowed"
		SSL_VHOST_FORCE_HTTPS="no-force-https"
	fi

	PHP_BACKEND_NAME_VARIABLE_NAME="SSL_VHOST_${i}_PHP_BACKEND_NAME"
	eval PHP_BACKEND_NAME=\$$PHP_BACKEND_NAME_VARIABLE_NAME
	PHP_BACKEND_PORT_VARIABLE_NAME="SSL_VHOST_${i}_PHP_BACKEND_PORT"
	eval PHP_BACKEND_PORT=\$$PHP_BACKEND_PORT_VARIABLE_NAME

        PROXY_BACKEND_NAME_VARIABLE_NAME="SSL_VHOST_${i}_PROXY_BACKEND_NAME"
        eval PROXY_BACKEND_NAME=\$$PROXY_BACKEND_NAME_VARIABLE_NAME
        PROXY_BACKEND_PORT_VARIABLE_NAME="SSL_VHOST_${i}_PROXY_BACKEND_PORT"
        eval PROXY_BACKEND_PORT=\$$PROXY_BACKEND_PORT_VARIABLE_NAME

}

setup_first_vhost_file_for_challenge() {
	CHALLENGE_FILE="/etc/nginx/sites-enabled/challenge_${SSL_VHOST}.conf"
	output "Copying temporary template challenge.tpl on $CHALLENGE_FILE"
	cp /challenge.tpl $CHALLENGE_FILE
	output "Setting up variables in the template"
	sed -i s/"%SSL_VHOST_LIST%"/"$SSL_VHOST_LIST"/g $CHALLENGE_FILE
	sed -i s~"%LETS_ENCRYPT_CHALLENGE_FILE_DIR%"~"$LETS_ENCRYPT_CHALLENGE_FILE_DIR"~g $CHALLENGE_FILE
}


setup_real_vhost_file() {
	VHOST_FILE="/etc/nginx/sites-enabled/${SSL_VHOST}.conf"
	case $SSL_VHOST_MODE in
	direct)
		output "\"$SSL_VHOST_MODE\" mode set up."
	;;
	php)
		output "\"$SSL_VHOST_MODE\" mode set up."
		if [ "$PHP_BACKEND_NAME" == "" ]
		then
			output "No PHP backend defined! You must set up env variable \"SSL_VHOST_${i}_PHP_BACKEND_NAME\" Can't continue. Exiting."
                	crash_container
		fi
		if [ "$PHP_BACKEND_PORT" == "" ]
		then
			output "No PHP backend port defined! You must set up env variable \"SSL_VHOST_${i}_PHP_BACKEND_PORT\" Can't continue. Exiting."
			crash_container
		fi
	;;
	proxy)
		output "\"$SSL_VHOST_MODE\" mode set up."
		if [ "$PROXY_BACKEND_NAME" == "" ]
		then
			output "No Proxy backend defined! You must set up env variable \"SSL_VHOST_${i}_PROXY_BACKEND_NAME\" Can't continue. Exiting."
			crash_container
		fi
		if [ "$PROXY_BACKEND_PORT" == "" ]
		then
			output "No Proxy backend port defined! You must set up env variable \"SSL_VHOST_${i}_PROXY_BACKEND_PORT\" Can't continue. Exiting."
			crash_container
		fi
	;;
	*)
		output "You didn't set up SSL_VHOST_MODE (direct, php or proxy). "
		output "Setting it by default to direct (put your html files on /var/www/${SSL_VHOST}/)"
		SSL_VHOST_MODE="direct"
        ;;
        esac
	TEMPLATE_FILE_NAME="${SSL_VHOST_MODE}_${SSL_VHOST_FORCE_HTTPS}.tpl"
	output "Copying template $TEMPLATE_FILE_NAME on $VHOST_FILE"
	cp /$TEMPLATE_FILE_NAME $VHOST_FILE
	output "Setting up variables in the template"
	sed -i s/"%SSL_VHOST%"/"$SSL_VHOST"/g $VHOST_FILE
	# If Vhots alt list is empty, generate a random string in order to not break the vhost file,
	#  and the server name will not be reached from outsite
	if [ "$SSL_VHOST_ALT_LIST" == "" ]
	then
		SSL_VHOST_ALT_LIST=`date | md5sum | awk '{print $1}'`
	fi
	sed -i s/"%SSL_VHOST_ALT_LIST%"/"$SSL_VHOST_ALT_LIST"/g $VHOST_FILE
	sed -i s~"%LETS_ENCRYPT_CHALLENGE_FILE_DIR%"~"$LETS_ENCRYPT_CHALLENGE_FILE_DIR"~g $VHOST_FILE
	sed -i s/"%PHP_BACKEND_NAME%"/"$PHP_BACKEND_NAME"/g $VHOST_FILE
	sed -i s/"%PHP_BACKEND_PORT%"/"$PHP_BACKEND_PORT"/g $VHOST_FILE
	sed -i s/"%PROXY_BACKEND_NAME%"/"$PROXY_BACKEND_NAME"/g $VHOST_FILE
	sed -i s/"%PROXY_BACKEND_PORT%"/"$PROXY_BACKEND_PORT"/g $VHOST_FILE
	output "Creating vhost rootdir"
	mkdir -p /var/www/${SSL_VHOST} && chown -R www-data:www-data /var/www/${SSL_VHOST}
}

create_cert() {
	output "Cert file not present. Creating it"
	setup_first_vhost_file_for_challenge
	reload_nginx
	DOMAINS_LIST_OPTS=`echo $SSL_VHOST_LIST | sed s/" "/" -d "/g`
	certbot certonly -n --webroot -w $LETS_ENCRYPT_CHALLENGE_FILE_DIR --agree-tos --email $SSL_VHOST_EMAIL --rsa-key-size $RSA_KEY_SIZE -d $DOMAINS_LIST_OPTS
	if [ $? -ne 0 ]; then
		output "ERROR Getting SSL certificate. Exit"
		crash_container
	fi
	clean_temp_vhost_file
}

process_vhost() {
	output "SSL_VHOST $SSL_VHOST declared in the env configuration."
	setup_variables
	if [ ! -f ${LETS_ENCRYPT_CERTS_DIR}/live/${SSL_VHOST}/fullchain.pem ];
	then
		create_cert
	else
		output "Cert file is present. no need to create it"
	fi
	setup_real_vhost_file
	reload_nginx
}

process_vhosts() {
	STOP=0
	i=1
	while [ $STOP -ne 1 ]
	do
		VHOST_VARIABLE_NAME="SSL_VHOST_$i"
		eval SSL_VHOST=\$$VHOST_VARIABLE_NAME
		if [ "$SSL_VHOST" != "" ]
		then
			process_vhost
		else
			STOP=1
		fi
		i=$[$i+1]
	done
}

##########################################################
##							##
##	Script begin					##
##							##
##########################################################

init
process_vhosts
