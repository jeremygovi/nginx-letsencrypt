#!/bin/bash

function output() {
        echo "$(date +"[%Y-%m-%d %T]") $1 "
}

output "Launching SSL Certs creation script"
./create-certs.sh &

output "Launching SSL Certs renewal daemon"
./renew-certs-daemon.sh &

output "Launching Nginx Server"
exec "$@"
