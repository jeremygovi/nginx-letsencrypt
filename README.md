<h1 id="nginx-letsencrypt">nginx-letsencrypt</h1>

<p>Nginx with a certbot in a same container</p>

<p align="center">
  <img src="https://www.nginx.com/wp-content/themes/nginx-theme/assets/img/logo.png" width="20%">
    <img src="https://geekeries.org/wp-content/uploads/2016/10/letsencrypt-logo-300.png" width="20%">
</p>

<hr>



<h2 id="why-this-container">Why this container?</h2>

<p>There are already solutions on the net to get automatic ssl on a docker stacks like <a href="https://traefik.io">traefik</a> or <a href="https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion">JrCs/docker-letsencrypt-nginx-proxy-companion</a> (lets-encrypt container), plus <a href="https://github.com/jwilder/nginx-proxy">jwilder/nginx-proxy</a> to proxify all queries to you backend. However, i would like to have only one container to handle this and avoid to declare environment variables on other containers to let all the stuff working. <br>
In addition to that, some options have been added on this container, like allowing http traffic or use a php backend.</p>

<p>This container is an <a href="https://www.nginx.com/">Nginx</a> instance with 2 scripts running, one at startup to request SSL Certs and install nginx vhost, the other run as a daemon and check for SSL Cert renewal every hour.</p>

<hr>

<h2 id="require">Require:</h2>

<ul>
<li>git (sudo apt-get install git)</li>
<li><a href="https://docs.docker.com/engine/installation/">docker</a> </li>
<li><a href="https://docs.docker.com/compose/install/">docker-compose</a> (recommended)</li>
</ul>

<hr>

<h2 id="how-to-use-it-quickly">How to use it quickly:</h2>

<ul>
<li>The simplest way:</li>
</ul>

<pre class="prettyprint"><code class="language-sh hljs ruby">docker run -d -p <span class="hljs-number">80</span><span class="hljs-symbol">:</span><span class="hljs-number">80</span> -p <span class="hljs-number">443</span><span class="hljs-symbol">:</span><span class="hljs-number">443</span> -v       - <span class="hljs-regexp">/path_to/your</span><span class="hljs-regexp">/html_files/</span><span class="hljs-symbol">:/var/www/<span class="hljs-string">" -e SSL_VHOST_1="</span>yourdomain</span>.com<span class="hljs-string">" -e SSL_VHOST_1_EMAIL="</span>you<span class="hljs-variable">@gmail</span>.com<span class="hljs-string">"
 jeremygovi/nginx-letsencrypt</span></code></pre>

<p>Then, wait few seconds, check the logs (docker logs <code>&lt;container_id&gt;</code>), put your html, js or any static files to <code>/path_to/your/html_files/</code> <br>
That’all :-)</p>



<h2 id="lets-configure-it-more-deeply">Let’s configure it more deeply:</h2>



<h3 id="volumes-to-mount">Volumes to mount</h3>

<ul>
<li>/etc/nginx/nginx.conf (optional) : if you want to add some parameters to nginx conf   </li>
<li>/etc/nginx/block_ip.conf (optional): if you want to blacklist some IP’s </li>
<li>/var/log/nginx/ (optional): if you want to persist youl logs</li>
<li>/etc/letsencrypt/ (optional but <strong>recommended</strong>): to persist your SSL Certificates. Be careful, there is a ratelimit asking for certs (max 20 times in a week)</li>
<li>/var/www/ (<strong>mandatory</strong> in <code>direct</code> and <code>php</code> mode): persist your static files</li>
</ul>

<h3 id="modes">Modes</h3>

<p>For the moment, this container supports 3 modes:</p>

<ul>
<li><p><code>direct</code>: This is when you just want to serve static files like images, html, js or a single page app for example </p></li>
<li><p><code>php</code>: This is when you want to use a <a href="https://gitlab.com/jeremygovi/php7-fpm">php-fpm</a> backend</p></li>
<li><p><code>proxy</code>: This is when you want to serve your cert and proxify to a backend (nodejs, haproxy, nginx, elasticsearch…) </p></li>
</ul>

<p>You can set up multiple Vhosts, just separate them by number. Ex: <code>SSL_VHOST_1="site1.com"</code>, <code>SSL_VHOST_2="other_site.com"</code></p>

<p>For each type, environment variables need to be declared (mandatory):</p>

<h4 id="direct"><strong>direct</strong></h4>

<table>
<thead>
<tr>
  <th>Environment variable name</th>
  <th>Required</th>
  <th>Default value</th>
  <th>About this var</th>
  <th>Example</th>
</tr>
</thead>
<tbody><tr>
  <td>SSL_VHOST_X</td>
  <td>true</td>
  <td>N.A</td>
  <td>The name of your main domain</td>
  <td>SSL_VHOST_1: “example.com”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_ALT</td>
  <td>false</td>
  <td>N.A</td>
  <td>Variants for your main domain. If multiple, separate them by a comma. Note: all your Alt names will be redirected to your main domain</td>
  <td>SSL_VHOST_1_ALT: “m.example.com, www.example.com, www.example.com”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_EMAIL</td>
  <td>true</td>
  <td>N.A</td>
  <td>Your Email. Notifications will be sent to this address if one of your cert is about to expire</td>
  <td>you@example.com</td>
</tr>
<tr>
  <td>SSL_VHOST_X_FORCE_HTTPS</td>
  <td>false</td>
  <td>no</td>
  <td>Force all http traffic to https</td>
  <td>SSL_VHOST_X_FORCE_HTTPS: “yes”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_MODE</td>
  <td>false</td>
  <td><code>direct</code></td>
  <td>Explained below</td>
  <td>SSL_VHOST_1_MODE: “direct”</td>
</tr>
</tbody></table>


<h4 id="php"><strong>php</strong></h4>

<table>
<thead>
<tr>
  <th>Environment variable name</th>
  <th>Required</th>
  <th>Default value</th>
  <th>About this var</th>
  <th>Example</th>
</tr>
</thead>
<tbody><tr>
  <td>SSL_VHOST_X</td>
  <td>true</td>
  <td>N.A</td>
  <td>The name of your main domain</td>
  <td>SSL_VHOST_1: “example.com”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_ALT</td>
  <td>false</td>
  <td>N.A</td>
  <td>Variants for your main domain. If multiple, separate them by a comma. Note: all your Alt names will be redirected to your main domain</td>
  <td>SSL_VHOST_1_ALT: “m.example.com, www.example.com, www.example.com”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_EMAIL</td>
  <td>true</td>
  <td>N.A</td>
  <td>Your Email. Notifications will be sent to this address if one of your cert is about to expire</td>
  <td>you@example.com</td>
</tr>
<tr>
  <td>SSL_VHOST_X_FORCE_HTTPS</td>
  <td>false</td>
  <td>no</td>
  <td>Force all http traffic to https</td>
  <td>SSL_VHOST_X_FORCE_HTTPS: “yes”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_MODE</td>
  <td>false</td>
  <td><code>direct</code></td>
  <td>Explained below</td>
  <td>SSL_VHOST_1_MODE: “php”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_PHP_BACKEND_NAME</td>
  <td>true</td>
  <td>N.A</td>
  <td>The php backend name to fastcgi_pass (use docker link like your_php_container:php-fpm).</td>
  <td>SSL_VHOST_1_PHP_BACKEND_NAME: “php-fpm”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_PHP_BACKEND_PORT</td>
  <td>true</td>
  <td>N.A</td>
  <td>The php backend port</td>
  <td>SSL_VHOST_1_PHP_BACKEND_PORT: “9000”</td>
</tr>
</tbody></table>


<h4 id="proxy"><strong>proxy</strong></h4>

<table>
<thead>
<tr>
  <th>Environment variable name</th>
  <th>Required</th>
  <th>Default value</th>
  <th>About this var</th>
  <th>Example</th>
</tr>
</thead>
<tbody><tr>
  <td>SSL_VHOST_X</td>
  <td>true</td>
  <td>N.A</td>
  <td>The name of your main domain</td>
  <td>SSL_VHOST_1: “example.com”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_ALT</td>
  <td>false</td>
  <td>N.A</td>
  <td>Variants for your main domain. If multiple, separate them by a comma. Note: all your Alt names will be redirected to your main domain</td>
  <td>SSL_VHOST_1_ALT: “m.example.com, www.example.com, www.example.com”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_EMAIL</td>
  <td>true</td>
  <td>N.A</td>
  <td>Your Email. Notifications will be sent to this address if one of your cert is about to expire</td>
  <td>you@example.com</td>
</tr>
<tr>
  <td>SSL_VHOST_X_FORCE_HTTPS</td>
  <td>false</td>
  <td>no</td>
  <td>Force all http traffic to https</td>
  <td>SSL_VHOST_X_FORCE_HTTPS: “yes”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_MODE</td>
  <td>false</td>
  <td><code>direct</code></td>
  <td>Explained below</td>
  <td>SSL_VHOST_1_MODE: “php”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_PROXY_BACKEND_NAME</td>
  <td>true</td>
  <td>N.A</td>
  <td>The backend name to proxy_pass (use docker link like your_backend_container:nodejs).</td>
  <td>SSL_VHOST_1_PROXY_BACKEND_NAME: “nodejs”</td>
</tr>
<tr>
  <td>SSL_VHOST_X_PROXY_BACKEND_PORT</td>
  <td>true</td>
  <td>N.A</td>
  <td>The backend port</td>
  <td>SSL_VHOST_1_PROXY_BACKEND_PORT: “9200”</td>
</tr>
</tbody></table>




<h2 id="examples-with-docker-compose">Examples (with docker-compose):</h2>

<p><code>docker-compose.yml</code> example for php backend:</p>

<pre class="prettyprint"><code class=" hljs haml">version: '2'
services:
  nginx-letsencrypt:
    image: jeremygovi/nginx-letsencrypt
    container_name: nginx-letsencrypt
    hostname: nginx-letsencrypt
    ports:
      -<span class="ruby"> <span class="hljs-string">"80:80"</span>
</span>      -<span class="ruby"> <span class="hljs-string">"443:443"</span>
</span>    links:
      -<span class="ruby"> php7-<span class="hljs-symbol">fpm:</span>php-fpm
</span>    volumes:
      -<span class="ruby"> <span class="hljs-string">"/config/nginx/nginx.conf:/etc/nginx/nginx.conf"</span>
</span>      -<span class="ruby"> <span class="hljs-string">"/config/nginx/block_ip.conf:/etc/nginx/block_ip.conf"</span>
</span>      -<span class="ruby"> <span class="hljs-string">"/logs/nginx/:/var/log/nginx/"</span>
</span>      -<span class="ruby"> <span class="hljs-string">"/letsencrypt/:/etc/letsencrypt/"</span>
</span>      -<span class="ruby"> <span class="hljs-string">"/var/www/:/var/www/"</span>
</span>    environment:
      SSL_VHOST_1: "test.jeremygovi.fr"
      SSL_VHOST_1_ALT: "test2.jeremygovi.fr"
      SSL_VHOST_1_EMAIL: "contact@jeremygovi.fr"
      SSL_VHOST_1_MODE: "php"
      SSL_VHOST_1_PHP_BACKEND_NAME: "php-fpm"
      SSL_VHOST_1_PHP_BACKEND_PORT: "9000"
      SSL_VHOST_1_FORCE_HTTPS: "yes"

  php7-fpm:
    image: jeremygovi/php7-fpm
    hostname: php7-fpm
    volumes:
    -<span class="ruby"> /logs/php-fpm/<span class="hljs-symbol">:/var/log/php-fpm/</span>
</span>    -<span class="ruby"> /var/www/<span class="hljs-symbol">:/var/www/</span>
</span></code></pre>