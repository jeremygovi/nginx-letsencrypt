FROM debian:jessie

ENV NGINX_VERSION 1.13.6-1~jessie

# Set the timezone.
RUN echo "Europe/Paris" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

# Basics
RUN apt-get update \
	&& apt-get install -y vim curl wget net-tools \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*

# Nginx
RUN  wget https://nginx.org/keys/nginx_signing.key -O - | apt-key add - \
	&& echo "deb http://nginx.org/packages/mainline/debian/ jessie nginx" >> /etc/apt/sources.list \
	&& apt-get update \
	&& apt-get install -y \
						nginx=${NGINX_VERSION} \
						nginx-module-xslt \
						nginx-module-geoip \
						nginx-module-image-filter \
						nginx-module-perl \
						nginx-module-njs \
						gettext-base \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /etc/nginx/sites-enabled

# Cerbot
RUN echo "deb http://ftp.debian.org/debian jessie-backports main" >> /etc/apt/sources.list \
	&& apt-get update \
	&& apt-get -y install certbot -t jessie-backports \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*

COPY ./scripts/* /
COPY ./vhost_templates/* /
COPY ./config/*.conf /etc/nginx/

EXPOSE 80 443

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD nginx -g "daemon off;"
